package com.mycompany.acom.controller;

import com.mycompany.acom.api.BookingHandler;
import com.mycompany.acom.domain.BookingHistory;
import com.mycompany.acom.exception.BookingNotFoundException;
import com.mycompany.acom.exception.RoomAlreadyReservedException;
import com.mycompany.acom.service.BookingService;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

public class BookingController implements BookingHandler {

    private final BookingService bookingService;
    
    public BookingController() {
        this.bookingService = new BookingService();
    }
    
    @Override
    public UUID book(UUID userId, UUID roomId, LocalDate startDate, LocalDate endDate, int roomPrice) throws RoomAlreadyReservedException {
        return bookingService.book(userId, roomId, startDate, endDate, roomPrice);
    }

    @Override
    public UUID cancel(UUID bookingHistoryId) throws BookingNotFoundException {
        return bookingService.cancel(bookingHistoryId);
    }

    @Override
    public List<BookingHistory> getUserBookingHistory(UUID userId) {
        return bookingService.getUserBookingHistory(userId);
    }

    @Override
    public void printBookingHistory(UUID userId) {
        bookingService.printBookingHistory(userId);
    }

    @Override
    public List<BookingHistory> filterBookingHistory(UUID userId, LocalDate startDate, LocalDate endDate, int price) {
        return bookingService.filterBookingHistory(userId, startDate, endDate, price);
    }
}