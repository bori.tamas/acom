package com.mycompany.acom.controller;

import com.mycompany.acom.api.RoomHandler;
import com.mycompany.acom.domain.Room;
import com.mycompany.acom.service.RoomService;
import java.time.LocalDate;
import java.util.List;

public class RoomController implements RoomHandler {

    private final RoomService roomService;
    
    public RoomController() {
        this.roomService = new RoomService();
    }

    @Override
    public List<Room> getAvailableRooms(int price, LocalDate startDate, LocalDate endDate) {
        return roomService.getAvailableRooms(price, startDate, endDate);
    }
}
