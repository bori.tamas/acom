package com.mycompany.acom.service;

import com.mycompany.acom.domain.BookingHistory;
import com.mycompany.acom.domain.Room;
import com.mycompany.acom.repository.BookingHistoryRepository;
import com.mycompany.acom.repository.RoomRepository;
import java.time.LocalDate;
import java.util.List;

public class RoomService {

    private final RoomRepository roomrepository;
    private final BookingHistoryRepository bookingHistoryrepository;
    
    public RoomService() {
        this.roomrepository = new RoomRepository();
        this.bookingHistoryrepository = new BookingHistoryRepository();
    }

    public List<Room> getAvailableRooms(int price, LocalDate startDate, LocalDate endDate) {        
        List<Room> rooms = roomrepository.getAvailableRooms(price);
        for(Room room: rooms) {
            List<BookingHistory> roomHistory = bookingHistoryrepository.getBookingHistoryByRoom(room.getRoomId());
            for(BookingHistory bh: roomHistory) {
                if ((bh.getStartDate().isBefore(startDate) && bh.getEndDate().isAfter(startDate)) 
                                            || (bh.getStartDate().isBefore(endDate) && bh.getEndDate().isAfter(endDate))) {
                    rooms.remove(room);
                    break;
                }
            }
        }
        
        return rooms;
    }
}