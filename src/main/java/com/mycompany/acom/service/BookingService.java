package com.mycompany.acom.service;

import com.mycompany.acom.domain.BookingHistory;
import com.mycompany.acom.exception.BookingNotFoundException;
import com.mycompany.acom.exception.RoomAlreadyReservedException;
import com.mycompany.acom.repository.BookingHistoryRepository;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class BookingService {

    private final BookingHistoryRepository bookingHistoryrepository;

    public BookingService() {
        this.bookingHistoryrepository = new BookingHistoryRepository();
    }

    public List<BookingHistory> getUserBookingHistory(UUID userId) {
        return bookingHistoryrepository.getBookingHistoryByUserId(userId);
    }

    public void printBookingHistory(UUID userId) {
        List<BookingHistory> bookings = getUserBookingHistory(userId);
        bookings.forEach((booking) -> {
            System.out.println(booking.toString());
        });
    }

    /*
     * Checks that there overlap between dates and also under the given price.
     */
    public List<BookingHistory> filterBookingHistory(UUID userId, LocalDate startDate, LocalDate endDate, int price) {
        List<BookingHistory> result = new ArrayList<>();
        List<BookingHistory> userBookings = getUserBookingHistory(userId);
        userBookings.stream().filter((bh) -> ((bh.getStartDate().isBefore(startDate) && bh.getEndDate().isAfter(startDate))
                || (bh.getStartDate().isBefore(endDate) && bh.getEndDate().isAfter(endDate)) && price >= bh.getBookingPrice())).forEachOrdered((bh) -> {
                    result.add(bh);
        });

        return result;
    }
    
    public UUID book(UUID userId, UUID roomId, LocalDate startDate, LocalDate endDate, int roomPrice) throws RoomAlreadyReservedException {
        List<BookingHistory> roomBookings = bookingHistoryrepository.getBookingHistoryByRoomId(roomId);
        boolean hasMatch = roomBookings.stream().anyMatch((bh) -> ((bh.getStartDate().isBefore(startDate) && bh.getEndDate().isAfter(startDate))
                || (bh.getStartDate().isBefore(endDate) && bh.getEndDate().isAfter(endDate))));
        if (!hasMatch){
            return this.bookingHistoryrepository.create(userId, roomId, startDate, endDate, roomPrice);
        } else {
            throw new RoomAlreadyReservedException(roomId);
        }
    }
    
    public UUID cancel(UUID bookingHistoryId) throws BookingNotFoundException {
        return this.bookingHistoryrepository.cancel(bookingHistoryId);
    }
}