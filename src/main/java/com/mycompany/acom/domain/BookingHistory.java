package com.mycompany.acom.domain;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.StringJoiner;
import java.util.UUID;

public class BookingHistory implements Serializable {
    
    private UUID bookingHistoryId;
    
    private UUID userId;
    
    private UUID accomodationId;
    
    private UUID roomId;
    
    private int bookingPrice;

    private LocalDate startDate;
    
    private LocalDate endDate;
    
    private boolean valid;

    public UUID getBookingHistoryId() {
        return bookingHistoryId;
    }

    public void setBookingHistoryId(UUID bookingHistoryId) {
        this.bookingHistoryId = bookingHistoryId;
    }

    public UUID getUserId() {
        return userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public UUID getAccomodationId() {
        return accomodationId;
    }

    public void setAccomodationId(UUID accomodationId) {
        this.accomodationId = accomodationId;
    }
    
    public UUID getRoomId() {
        return roomId;
    }

    public void setRoomId(UUID roomId) {
        this.roomId = roomId;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }
    
    public int getBookingPrice() {
        return bookingPrice;
    }

    public void setBookingPrice(int bookingPrice) {
        this.bookingPrice = bookingPrice;
    }
    
    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }
    
    @Override
    public String toString() {
        return new StringJoiner(", ", BookingHistory.class.getSimpleName() + "[", "]")
            .add("userId=" + userId)
            .add("accomodationId=" + accomodationId)
            .add("roomId=" + roomId)
            .add("startDate=" + startDate)
            .add("endDate=" + endDate)
            .toString();
    }
}