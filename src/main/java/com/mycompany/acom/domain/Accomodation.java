package com.mycompany.acom.domain;

import java.io.Serializable;
import java.util.UUID;

public class Accomodation implements Serializable {
    
    private UUID accomodationId;
    
    private String name;
    
    private String address;

    public UUID getAccomodationId() {
        return accomodationId;
    }

    public void setAccomodationId(UUID accomodationId) {
        this.accomodationId = accomodationId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
