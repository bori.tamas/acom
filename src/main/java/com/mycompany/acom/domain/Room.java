package com.mycompany.acom.domain;

import java.io.Serializable;
import java.util.UUID;

public class Room implements Serializable {

    private UUID roomId;
    
    private UUID accomodationId;
    
    private int number;
    
    private int floor;
    
    private int price;
    
    public UUID getRoomId() {
        return roomId;
    }

    public void setRoomId(UUID roomId) {
        this.roomId = roomId;
    }
    
    public UUID getAccomodationId() {
        return accomodationId;
    }

    public void setAccomodationId(UUID accomodationId) {
        this.accomodationId = accomodationId;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getFloor() {
        return floor;
    }

    public void setFloor(int floor) {
        this.floor = floor;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
