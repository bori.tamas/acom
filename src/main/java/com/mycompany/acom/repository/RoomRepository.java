package com.mycompany.acom.repository;

import com.mycompany.acom.domain.Room;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/*
 * Dummy Data repository.
 **/
public class RoomRepository {
    
    private final Room room1;
    private final Room room2;
    private final Room room3;
    private final Room room4;
    private final Room room5;
    
    private List<Room> rooms;
    
    public RoomRepository() {
        
        rooms = new ArrayList<>();
        
        this.room1 = new Room();
        this.room1.setRoomId(UUID.randomUUID());
        this.room1.setFloor(1);
        this.room1.setNumber(1);
        this.room1.setPrice(1000);
        rooms.add(room1);
        
        this.room2 = new Room();
        this.room2.setRoomId(UUID.randomUUID());
        this.room2.setFloor(1);
        this.room2.setNumber(2);
        this.room2.setPrice(11000);
        rooms.add(room2);
        
        this.room3 = new Room();
        this.room3.setRoomId(UUID.randomUUID());
        this.room3.setFloor(2);
        this.room3.setNumber(1);
        this.room3.setPrice(3600);
        rooms.add(room3);
        
        this.room4 = new Room();
        this.room4.setRoomId(UUID.randomUUID());
        this.room4.setFloor(2);
        this.room4.setNumber(2);
        this.room4.setPrice(2000);
        rooms.add(room4);
        
        this.room5 = new Room();
        this.room5.setRoomId(UUID.randomUUID());
        this.room5.setFloor(3);
        this.room5.setNumber(1);
        this.room5.setPrice(1500);
        rooms.add(room5);
    }
    
    public List<Room> getAvailableRooms(int price) {
        List<Room> result = new ArrayList<>();
        rooms.stream().filter((room) -> (room.getPrice() <= price)).forEachOrdered((room) -> {
            result.add(room);
        });
        
        return result;
    }
}