package com.mycompany.acom.repository;

import com.mycompany.acom.domain.BookingHistory;
import com.mycompany.acom.exception.BookingNotFoundException;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class BookingHistoryRepository {
    private final BookingHistory bookingHistory1;
    private final BookingHistory bookingHistory2;
    private final BookingHistory bookingHistory3;
    private final BookingHistory bookingHistory4;
    private final BookingHistory bookingHistory5;
    
    private List<BookingHistory> bookings;

    public BookingHistoryRepository() {
        this.bookingHistory1 = new BookingHistory();
        this.bookingHistory1.setBookingHistoryId(UUID.randomUUID());
        this.bookingHistory1.setAccomodationId(UUID.randomUUID());
        this.bookingHistory1.setUserId(UUID.randomUUID());
        this.bookingHistory1.setStartDate(LocalDate.MAX);
        this.bookingHistory1.setEndDate(LocalDate.MAX);
        bookings.add(bookingHistory1);
        
        this.bookingHistory2 = new BookingHistory();
        this.bookingHistory2.setBookingHistoryId(UUID.randomUUID());
        this.bookingHistory2.setAccomodationId(UUID.randomUUID());
        this.bookingHistory2.setUserId(UUID.randomUUID());
        this.bookingHistory2.setEndDate(LocalDate.MAX);
        bookings.add(bookingHistory2);
        
        this.bookingHistory3 = new BookingHistory();
        this.bookingHistory3.setBookingHistoryId(UUID.randomUUID());
        this.bookingHistory3.setAccomodationId(UUID.randomUUID());
        this.bookingHistory3.setUserId(UUID.randomUUID());
        this.bookingHistory3.setStartDate(LocalDate.MAX);
        this.bookingHistory3.setEndDate(LocalDate.MAX);
        bookings.add(bookingHistory3);
        
        this.bookingHistory4 = new BookingHistory();
        this.bookingHistory4.setBookingHistoryId(UUID.randomUUID());
        this.bookingHistory4.setAccomodationId(UUID.randomUUID());
        this.bookingHistory4.setUserId(UUID.randomUUID());
        this.bookingHistory4.setStartDate(LocalDate.MAX);
        this.bookingHistory4.setEndDate(LocalDate.MAX);
        bookings.add(bookingHistory4);
        
        this.bookingHistory5 = new BookingHistory();
        this.bookingHistory5.setBookingHistoryId(UUID.randomUUID());
        this.bookingHistory5.setAccomodationId(UUID.randomUUID());
        this.bookingHistory5.setUserId(UUID.randomUUID());
        this.bookingHistory5.setStartDate(LocalDate.MAX);
        this.bookingHistory5.setEndDate(LocalDate.MAX);
        bookings.add(bookingHistory5);
    }
    
    public List<BookingHistory> getBookingHistoryByRoom(UUID roomId) {
        List<BookingHistory> result = new ArrayList<>();
        bookings.stream().filter((bh) -> (bh.getRoomId().equals(roomId))).forEachOrdered((bh) -> {
            result.add(bh);
        });
        
        return result;
    }
    
    public List<BookingHistory> getBookingHistoryByUserId(UUID userId) {
        List<BookingHistory> result = new ArrayList<>();
        bookings.stream().filter((bh) -> (bh.getUserId().equals(userId))).forEachOrdered((bh) -> {
            result.add(bh);
        });
        
        return result;
    }
    
    public List<BookingHistory> getBookingHistoryByRoomId(UUID roomId) {
        List<BookingHistory> result = new ArrayList<>();
        bookings.stream().filter((bh) -> (bh.getRoomId().equals(roomId))).forEachOrdered((bh) -> {
            result.add(bh);
        });
        
        return result;
    }
    
    public UUID create(UUID userId, UUID roomId, LocalDate startDate, LocalDate endDate, int roomPrice) {
        BookingHistory newBooking = new BookingHistory();
        newBooking.setBookingHistoryId(UUID.randomUUID());
        newBooking.setAccomodationId(UUID.randomUUID());
        newBooking.setRoomId(roomId);
        newBooking.setUserId(userId);
        newBooking.setStartDate(startDate);
        newBooking.setEndDate(endDate);
        Period p = Period.between(startDate, endDate);
        newBooking.setBookingPrice(p.getDays() * roomPrice);
        newBooking.setValid(true);
        this.bookings.add(newBooking);
        
        return newBooking.getBookingHistoryId();
    }
    
    public UUID cancel(UUID bookingHistoryId) throws BookingNotFoundException {
        for (BookingHistory booking: this.bookings) {
            if (booking.getBookingHistoryId().equals(bookingHistoryId)) {
                booking.setValid(false);
                return booking.getBookingHistoryId();
            }
        }
        
        throw new BookingNotFoundException(bookingHistoryId);
    }
}