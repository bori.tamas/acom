package com.mycompany.acom.api;

import com.mycompany.acom.domain.Room;
import java.time.LocalDate;
import java.util.List;

public interface RoomHandler {
    
    public List<Room> getAvailableRooms(int price, LocalDate startDate, LocalDate endDate);
}