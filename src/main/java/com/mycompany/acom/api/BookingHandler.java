package com.mycompany.acom.api;

import com.mycompany.acom.domain.BookingHistory;
import com.mycompany.acom.exception.BookingNotFoundException;
import com.mycompany.acom.exception.RoomAlreadyReservedException;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

public interface BookingHandler {
    
    /**
     * The user can book a new room.
     * 
     * @param userId the id of the user
     * @param roomId the id of the room
     * @param startDate start date of the booking
     * @param endDate end date of the booking
     * @param roomPrice price of the room
     * @return the id of the new booking
     */
    public UUID book(UUID userId, UUID roomId, LocalDate startDate, LocalDate endDate, int roomPrice) throws RoomAlreadyReservedException;
    
    /**
     * The user can cancel a booking.
     * 
     * @param bookingHistoryId the id of the booking
     * @return the booking id if the cancel was successfull, otherwise throws an exception
     * @throws BookingNotFoundException
     */
    public UUID cancel(UUID bookingHistoryId) throws BookingNotFoundException;
    
    /**
     * The user can see his/her bookings.
     * 
     * @param userId the id of the user
     * @return the list of bookings for the user
     */
    public List<BookingHistory> getUserBookingHistory(UUID userId);
    
    /**
     * The user can print his/her bookings to the console.
     * 
     * @param userId the id of the user
     */
    public void printBookingHistory(UUID userId);
    
    /**
     * the user can filter her/his bookings.
     * 
     * @param userId the id of the user
     * @param startDate start date of the booking
     * @param endDate end date of the booking
     * @param price the price of the booking
     * @return a filtered list of booking history entities. 
     */
    public List<BookingHistory> filterBookingHistory(UUID userId, LocalDate startDate, LocalDate endDate, int price);
}