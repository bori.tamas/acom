package com.mycompany.acom.exception;

import java.util.UUID;

public class RoomAlreadyReservedException extends RuntimeException {
    
    /**
     * Exception to be thrown if the given room is not found.
     */
    public RoomAlreadyReservedException(UUID roomId) {
        super("The given room is not found: " + roomId);
    }
}
