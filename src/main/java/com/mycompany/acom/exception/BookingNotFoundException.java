package com.mycompany.acom.exception;

import java.util.UUID;

public class BookingNotFoundException extends RuntimeException {
    
    /**
     * Exception to be thrown if the given booking is not found.
     */
    public BookingNotFoundException(UUID bookingHistoryId) {
        super("The given booking is not found: " + bookingHistoryId);
    }
}
